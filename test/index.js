const _ = require('lodash');
const { expect } = require('chai');

const { timeoutPromise, isPromise } = require('../src');
const testTimeout = 1500;

function unixTimestamp(d) {
    if (_.isUndefined(d)) {
        d = new Date();
    }
    return Math.floor(d.getTime() / 1000);
}

describe('timeoutPromise()', () => {
    it('should return a promise that waits', async () => {
        const startTime = unixTimestamp();
        const endTime = await timeoutPromise(testTimeout).then(() => unixTimestamp());
        expect(endTime).to.be.greaterThan(startTime);
        expect(endTime - startTime).to.be.at.least(1);
    });
});

describe('isPromise()', () => {
    function expectPromise(v, shouldBeTrue = true) {
        return expect(isPromise(v)).to.equal(shouldBeTrue);
    }
    it('should recognize resolved promises', () => {
        const p = Promise.resolve();
        expectPromise(p);
    });
    it('should recognize rejected promises', () => {
        const p = Promise.reject()
            .catch((e) => console.error(e));
        expectPromise(p);
    });
    it('should recognize constructed promises', () => {
        let p = new Promise((resolve, reject) => {
            timeoutPromise(testTimeout)
                .then(() => resolve())
                .catch((e) => reject(e));
        });
        p = p.catch((e) => console.error(e));
        expectPromise(p);
    });
    it('should recognize promises from async functions', () => {
        async function f() {
            await timeoutPromise(testTimeout);
            return true;
        }

        expectPromise(f());
    });
    it('should not recognize normal things as promises', () => {
        const values = [
            true,
            false,
            {},
            { foo: 'bar' },
            0, 1, -1,
            1.1
        ];
        values.forEach((v) => expectPromise(v, false));
    });
});

describe('asPromise()', () => {
    const { asPromise } = require('../src');
    it('should return promise for passed in promises', async () => {
        let p = Promise.resolve('foo');
        p = asPromise(p);
        expect(p).to.be.instanceOf(Promise);
        expect(await p).to.equal('foo');
    });
    it('should return promise for passed in normal values', async () => {
        const p = asPromise('foo');
        expect(p).to.be.instanceOf(Promise);
        expect(await p).to.equal('foo');
    });
})

describe('asPromiseFn()', () => {
    const { asPromiseFn } = require('../src');
    it('should return promise for passed in promise', async () => {
        const f = asPromiseFn(_.identity);
        const p = f('foo');
        expect(p).to.be.instanceOf(Promise);
        expect(await p).to.equal('foo');
    });
    it('should return promise for passed in value', async () => {
        const f = asPromiseFn((v) => Promise.resolve(v));
        const p = f('foo');
        expect(p).to.be.instanceOf(Promise);
        expect(await p).to.equal('foo');
    });
});
