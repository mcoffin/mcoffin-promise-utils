const _ = require('lodash');

function timeoutPromise(t) {
    return new Promise((resolve, reject) => {
        return setTimeout(() => resolve(), t);
    });
}

function isPromise(v, promiseClasses = [Promise], checkThen = true) {
    if (!_.isObject(v)) {
        return false;
    }
    for (let c of promiseClasses) {
        if (v instanceof c) {
            return true;
        }
    }
    if (checkThen && _.isFunction(v.then)) {
        return true;
    }
    return false;
}

function asPromise(v, reject = false) {
    if (isPromise(v)) {
        return v;
    } else if (reject) {
        return Promise.reject(v);
    } else {
        return Promise.resolve(v);
    }
}

function asPromiseFn(f) {
    return (...args) => asPromise(f(...args));
}

module.exports = {
    timeoutPromise,
    isPromise,
    asPromise,
    asPromiseFn,
};
