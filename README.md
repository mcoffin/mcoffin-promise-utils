# @mcoffin/promise-utils

![NPM](https://img.shields.io/npm/l/@mcoffin/promise-utils)
![NPM Version](https://img.shields.io/npm/v/@mcoffin/promise-utils)

(Yet another) collection of JavaScript promise utilities for both [NodeJS](https://nodejs.org) and browser platforms.
